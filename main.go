package main

import (
	"flag"
	"fmt"
	"log"
	"os"
)

var openUrlCmd = map[string]string{
	"windows": "start",
	"linux":   "xdg-open",
	"darwin":  "open",
}

var openPngCmd = map[string]string{
	"windows": "",
	"linux":   "xdg-open",
	"darwin":  "open",
}

func main() {
	var username = flag.String("username", "", "hackernews username")
	var password = flag.String("password", "", "hackernews password")
	if os.Getenv("HN_USERNAME") != "" {
		*username = os.Getenv("HN_USERNAME")
	}
	if os.Getenv("HN_PASSWORD") != "" {
		*password = os.Getenv("HN_PASSWORD")
	}
	var debug = flag.Bool("debug", false, "show debug window")
	flag.Parse()

	hnInst := HN{}
	if *username != "" {
		err := hnInst.InitWithLogin(*username, *password)
		ifErrorExit(err)
	} else {
		err := hnInst.Init()
		ifErrorExit(err)
	}

	if err := hnGui(&hnInst, debug); err != nil {
		log.Panicln(err)
	}

}

func ifErrorExit(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
