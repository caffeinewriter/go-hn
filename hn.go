package main

import (
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/sclevine/agouti"
)

const (
	hnUrl      = "https://news.ycombinator.com/"
	hnLoginUrl = hnUrl + "login"
	hnNewsUrl  = hnUrl + "news"
)

// struct of hacker news site
type HN struct {
	page     *agouti.Page
	driver   *agouti.WebDriver
	items    []item
	username string
	loggedIn bool
}

// struct that represents a news
type item struct {
	rank  int
	id    string
	title string
	url   string
	site  string
}

func (hn *HN) setupDriver() (err error) {
	hn.driver = agouti.PhantomJS()
	hn.driver.Start()
	page, err := hn.driver.NewPage()
	if err != nil {
		return err
	}
	page.SetImplicitWait(20000)
	page.Size(1366, 768)
	hn.page = page
	if url, err := hn.page.URL(); err != nil {
		return nil
	} else {
		if url != hnUrl {
			hn.page.Navigate(hnUrl)
		}
	}
	return nil
}

func (hn *HN) login(username, password string) (err error) {
	if url, err := hn.page.URL(); err != nil {
		return nil
	} else {
		if url != hnUrl {
			hn.page.Navigate(hnUrl)
		}
	}
	hn.page.FindByLink("login").Click()
	hn.page.FirstByName("acct").SendKeys(username)
	hn.page.FirstByName("pw").SendKeys(password)
	hn.page.FindByButton("login").Click()
	hn.username = username
	hn.loggedIn = true
	return nil
}

func (hn *HN) clearItems() {
	hn.items = nil
}

func (hn *HN) parseItems() (err error) {
	body, err := hn.page.HTML()
	if err != nil {
		return err
	}

	doc, err := goquery.NewDocumentFromReader(strings.NewReader(body))
	if err != nil {
		return err
	}

	doc.Find(".athing").Each(func(i int, s *goquery.Selection) {
		title := strings.TrimSpace(s.Find("td.title>a").Text())
		url, exists := s.Find("td.title>a").Attr("href")
		if exists {
			url = strings.TrimSpace(url)
		}
		id, exists := s.Find(".votelinks a").Attr("id")
		if exists {
			id = strings.Split(id, "_")[1]
		}
		rankStr := strings.TrimSpace(s.Find("td.title>span.rank").Text())
		rank, _ := strconv.Atoi(strings.Replace(rankStr, ".", "", 1))
		site := s.Find("td.title>span>a").Text()
		hn.items = append(hn.items, item{
			id: id, title: title, url: url, rank: rank, site: site,
		})
	})

	// fmt.Printf("%v\n", hn.items)

	return nil
}

func (hn *HN) InitWithLogin(username, password string) (err error) {
	if err := hn.setupDriver(); err != nil {
		return err
	}
	if err := hn.login(username, password); err != nil {
		return err
	}
	if err := hn.parseItems(); err != nil {
		return err
	}

	return nil
}

func (hn *HN) Init() (err error) {
	if err := hn.setupDriver(); err != nil {
		return err
	}
	if err := hn.parseItems(); err != nil {
		return err
	}

	return nil
}

func (hn *HN) More() (err error) {
	hn.page.FindByLink("More").Click()
	hn.items = nil
	return hn.parseItems()
}
