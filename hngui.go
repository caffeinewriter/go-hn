package main

import (
	"fmt"
	"log"
	"os/exec"
	"runtime"
	"strings"

	"github.com/jroimartin/gocui"
)

var hn *HN
var debugView *gocui.View
var debugOn bool

func hnGui(hninst *HN, debug *bool) error {
	hn = hninst
	debugOn = *debug

	g := gocui.NewGui()
	if err := g.Init(); err != nil {
		log.Panicln(err)
	}
	defer g.Close()

	g.SetLayout(layout)
	if err := keybindings(g); err != nil {
		log.Panicln(err)
	}
	// g.SelBgColor = gocui.ColorBlack
	// g.SelFgColor = gocui.ColorGreen
	g.Cursor = true

	if err := g.MainLoop(); err != nil && err != gocui.ErrQuit {
		log.Panicln(err)
	}

	return nil
}

func debugLog(msg string) {
	if debugView != nil {
		logInView(debugView, msg)
	}
}

func logInView(v *gocui.View, msg string) {
	fmt.Fprintln(v, msg)
}

func cursorDown(g *gocui.Gui, v *gocui.View) error {
	debugLog("cursorDown")
	if v != nil {
		cx, cy := v.Cursor()
		if cy == len(hn.items) {
			return nil
		}
		if err := v.SetCursor(cx, cy+1); err != nil {
			return err
		}
	}
	return nil
}

func cursorUp(g *gocui.Gui, v *gocui.View) error {
	debugLog("cursorUp")
	if v != nil {
		cx, cy := v.Cursor()
		if cy == 1 {
			return nil
		}
		if err := v.SetCursor(cx, cy-1); err != nil && cy > 0 {
			return err
		}
	}
	return nil
}

func openItem(g *gocui.Gui, v *gocui.View) error {
	_, cy := v.Cursor()
	item := hn.items[cy-1]
	debugLog("openItem - " + item.title)
	if err := exec.Command(openUrlCmd[runtime.GOOS], item.url).Run(); err != nil {
		hn.driver.Stop()
		log.Panicln("unable to open news")
	}

	return nil
}

func upvote(g *gocui.Gui, v *gocui.View) error {
	_, cy := v.Cursor()
	item := hn.items[cy-1]
	debugLog("upvote - " + item.title)
	if hn.loggedIn {
		go func() { hn.page.FindByID("up_" + item.id).Click() }()
	}

	return nil
}

func nextPage(g *gocui.Gui, v *gocui.View) error {
	hn.page.FindByLink("More").Click()
	debugLog("nextPage")
	hn.clearItems()
	if err := hn.parseItems(); err != nil {
		hn.driver.Stop()
		log.Panicln(err)
	}
	debugLog("parsing new items done")
	maxX, maxY := v.Size()
	renderItems(v, maxX, maxY)
	if err := v.SetCursor(0, 1); err != nil {
		return err
	}
	return nil
}

func prevPage(g *gocui.Gui, v *gocui.View) error {
	hn.page.Back()
	debugLog("nextPage")
	hn.clearItems()
	if err := hn.parseItems(); err != nil {
		hn.driver.Stop()
		log.Panicln(err)
	}
	debugLog("parsing new items done")
	maxX, maxY := v.Size()
	renderItems(v, maxX, maxY)
	if err := v.SetCursor(0, 1); err != nil {
		return err
	}
	return nil
}

func quit(g *gocui.Gui, v *gocui.View) error {
	hn.driver.Stop()
	return gocui.ErrQuit
}

func clearLog(g *gocui.Gui, v *gocui.View) error {
	debugView.Clear()
	return nil
}

func keybindings(g *gocui.Gui) error {
	if err := g.SetKeybinding("HN", gocui.KeyArrowDown, gocui.ModNone, cursorDown); err != nil {
		return err
	}
	if err := g.SetKeybinding("HN", gocui.KeyArrowUp, gocui.ModNone, cursorUp); err != nil {
		return err
	}
	if err := g.SetKeybinding("HN", 'k', gocui.ModNone, cursorDown); err != nil {
		return err
	}
	if err := g.SetKeybinding("HN", 'j', gocui.ModNone, cursorUp); err != nil {
		return err
	}
	if err := g.SetKeybinding("", gocui.KeyCtrlC, gocui.ModNone, quit); err != nil {
		return err
	}
	if err := g.SetKeybinding("", gocui.KeyEsc, gocui.ModNone, quit); err != nil {
		return err
	}
	if err := g.SetKeybinding("", 'q', gocui.ModNone, quit); err != nil {
		return err
	}
	if err := g.SetKeybinding("", gocui.KeyCtrlL, gocui.ModNone, clearLog); err != nil {
		return err
	}
	if err := g.SetKeybinding("HN", gocui.KeyEnter, gocui.ModNone, openItem); err != nil {
		return err
	}
	if err := g.SetKeybinding("HN", gocui.KeySpace, gocui.ModNone, openItem); err != nil {
		return err
	}
	if err := g.SetKeybinding("HN", gocui.KeyCtrlU, gocui.ModNone, upvote); err != nil {
		return err
	}
	if err := g.SetKeybinding("HN", gocui.KeyArrowRight, gocui.ModNone, nextPage); err != nil {
		return err
	}
	if err := g.SetKeybinding("HN", gocui.KeyArrowLeft, gocui.ModNone, prevPage); err != nil {
		return err
	}
	if err := g.SetKeybinding("HN", 'l', gocui.ModNone, nextPage); err != nil {
		return err
	}
	if err := g.SetKeybinding("HN", 'h', gocui.ModNone, prevPage); err != nil {
		return err
	}
	if err := g.SetKeybinding("HN", gocui.KeyCtrlD, gocui.ModNone, logItems); err != nil {
		return err
	}
	return nil
}

func logItems(g *gocui.Gui, v *gocui.View) error {
	for _, item := range hn.items {
		debugLog(fmt.Sprintf("%d - %s", item.rank, item.title))
	}
	return nil
}

func layout(g *gocui.Gui) error {
	maxX, maxY := g.Size()
	if debugOn {
		if v, err := g.SetView("debug", 0, 32, maxX-1, maxY-1); err != nil {
			if err != gocui.ErrUnknownView {
				return err
			}
			v.Title = "Debug"
			v.Highlight = true
			v.Editable = true
			debugView = v
		}
	}
	if v, err := g.SetView("HN", -1, -1, maxX, 31); err != nil {
		if err != gocui.ErrUnknownView {
			return err
		}
		v.Highlight = true
		v.SelBgColor = gocui.ColorBlue
		v.SelFgColor = gocui.ColorWhite
		renderItems(v, maxX, maxY)
		if err := g.SetCurrentView("HN"); err != nil {
			return err
		}
		if err := v.SetCursor(0, 1); err != nil {
			return err
		}
	}

	return nil
}

func renderItems(v *gocui.View, maxX, maxY int) {
	debugLog("rendering Items")
	v.Clear()
	loginLine := "not logged in"
	if hn.loggedIn {
		loginLine = "logged in as " + hn.username
	}
	leftPadCount := maxX/2 - 6
	betPadCount := maxX/2 - len(loginLine) - 5
	head := strings.Repeat(" ", leftPadCount) + "HACKER NEWS" + strings.Repeat(" ", betPadCount) + loginLine
	fmt.Fprintln(v, head)
	for _, item := range hn.items {
		fmt.Fprintln(v, item.rank, " - ", item.title, "("+item.site+")")
	}
	debugLog("rendering Items done")
}
